﻿using SuccessiveDotNetTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SuccessiveDotNetTest.Controllers
{
    public class CandidateActionController : Controller
    {
        masterEntities EntityObject = new masterEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            //var getResult = EntityObject.Candidates.ToList();
            //SelectList list = new SelectList(getResult, "Id", "FirstName");
            //ViewBag.candidateList = list;
            //return View();

            masterEntities EntityObject = new masterEntities();
            ViewBag.CandidateList = new SelectList((from m in EntityObject.Candidates.ToList()
                                               select new
                                               {
                                                   Id = m.Id,
                                                   FullName = m.FirstName + " " + m.LastName + "(" + m.Email_Id + ")"
                                               }), 
                                               "Id","FullName", null);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CombinedDataModels candidate)
        {
            //For inserting the value into the candidate table
            Candidate frshcandidate = new Candidate();
            Interview_Schdeule schedule = new Interview_Schdeule();
            // var x = EntityObject.Candidates.OrderByDescending(m => m.Id).FirstOrDefault();
            //int id = x.Id+1;
            frshcandidate.FirstName = candidate.FirstName;
            frshcandidate.LastName = candidate.LastName;
            frshcandidate.Email_Id = candidate.Email_Id;
            frshcandidate.DOB = candidate.DateofBirth;
            frshcandidate.Mobile = candidate.Mobile;
            frshcandidate.Experience = candidate.TotalExperience;
            EntityObject.Candidates.Add(frshcandidate);
            EntityObject.SaveChanges();
            var x = EntityObject.Candidates.OrderByDescending(m => m.Id).FirstOrDefault();
            int id = x.Id;
            schedule.CandidateID = id;
            schedule.Scheduled_Date = candidate.Date;
            schedule.TimeFrom = candidate.From;
            schedule.TimeTo = candidate.To;
            schedule.Interviewer_Name = candidate.InterviewerName;
            EntityObject.Interview_Schdeule.Add(schedule);
            EntityObject.SaveChanges();
            return View(candidate);
        }
        public ActionResult ScheduleInterviewDetails()
        {
            using (masterEntities db = new masterEntities())
            {
                List<Candidate> canObj = db.Candidates.ToList();
                List<Interview_Schdeule> detailsObj = db.Interview_Schdeule.ToList();
                var scheduledRecord = from c in canObj
                join d in detailsObj on c.Id equals d.CandidateID into table1
                                     from d in table1.ToList()
                                      select new ViewDataModel
                                      {
                                          candidate = c,
                                          schdeule = d
                                      };

                return View(scheduledRecord);
            }
        }
    }
}
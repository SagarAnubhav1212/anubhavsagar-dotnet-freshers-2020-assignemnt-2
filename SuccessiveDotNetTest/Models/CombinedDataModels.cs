﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuccessiveDotNetTest.Models
{
    public class CombinedDataModels
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50,ErrorMessage ="Must be only 50 Characters only")]
        [DisplayName("First Name")]
        [RegularExpression("([a-zA-Z]+)", ErrorMessage = "Only Alphabets are Required")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Must be only 50 Characters only")]
        [DisplayName("Last Name")]
        [RegularExpression("([a-zA-Z]+)", ErrorMessage = "Only Alphabets are Required")]
        public string LastName { get; set; }
        [Required]
        [DisplayName("Date of Birth")]
        [DataType(DataType.Date),DisplayFormat(DataFormatString= "{0:dd/MM/yyyy}",ApplyFormatInEditMode =true)]
        public System.DateTime DateofBirth { get; set; }
        [Required]
        public int TotalExperience { get; set; }
        [Required]
        [StringLength(10, ErrorMessage = "Should be only 10 Digits Numbers")]
        [DisplayName("Mobile")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Only Digits are Required")]
        public string Mobile { get; set; }
        [Required]
        [DisplayName("Email_Id")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        [DataType(DataType.EmailAddress)]
        public string Email_Id { get; set; }
        [Required]
        [DisplayName("Date")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime Date { get; set; }
        [Required]
        [DisplayName("Time From")]
        [DataType(DataType.Time),DisplayFormat(ApplyFormatInEditMode = true)]
        public System.TimeSpan From { get; set; }
        [Required]
        [DisplayName("Time To")]
        [DataType(DataType.Time), DisplayFormat(ApplyFormatInEditMode = true)]
        public System.TimeSpan To { get; set; }
        [Required]
        [StringLength(100)]
        [DisplayName("Interviwer Name")]
        [RegularExpression("([a-zA-Z]+)", ErrorMessage = "Only Alphabets are Required")]
        public string InterviewerName { get; set; }

    }
}